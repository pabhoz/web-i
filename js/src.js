var string = '{\
	"musicApp":[\
	{\
		"id":1,\
		"nombre": "Starstrukk",\
		"artista": "3OH!3 Ft. Katy Perry",\
		"album":"Want",\
		"uri":"./audio/Starstrukk.mp3",\
		"caratula":"./covers/want.jpg"\
	},\
	{\
		"id":2,\
		"nombre": "Back in Black",\
		"artista": "AC/DC",\
		"album":"Back in Black",\
		"uri":"./audio/Back In Black - ACDC.mp3",\
		"caratula":"./covers/backInBlack.jpg"\
	},\
	{\
		"id":3,\
		"nombre": "Paranoid",\
		"artista": "Black Sabbath",\
		"album":"100% Rock, Vol 2",\
		"uri":"./audio/paranoid.mp3",\
		"caratula":"./covers/paranoid.jpg"\
	},\
	{\
		"id":4,\
		"nombre": "This War Is Ours",\
		"artista": "Escape The Fate",\
		"album":"This War Is Ours",\
		"uri":"./audio/This War Is Ours.mp3",\
		"caratula":"./covers/ThisWarIsOurs.jpg"\
	},\
	{\
		"id":5,\
		"nombre": "New Thang",\
		"artista": "Red Foo",\
		"album":"New Thang",\
		"uri":"./audio/NewThang.mp3",\
		"caratula":"./covers/NewThang.jpg"\
	}\
	]\
}';

var objJSON = JSON.parse(string);
var musicApp = objJSON.musicApp;
console.log(Object.keys(musicApp).length);

function cargarApp(){
	llenarLista(musicApp);
}

function llenarLista(musicList){

	var lista = document.getElementById('lista');
	for (var i = 0; i < Object.keys(musicList).length; i++) {
		lista.innerHTML += '<div class="song" data-uri="'+musicList[i].uri+'" data-cover="'+musicList[i].caratula+'">'+musicList[i].nombre+' - '+musicList[i].artista+'</div>';
	}

	var songs = lista.getElementsByClassName('song');
	var player = document.getElementById("player").getElementsByTagName('audio')[0];
	var cover = document.getElementById("player").getElementsByTagName("img")[0];
	for (var i = 0; i < songs.length; i++) {
		songs[i].onclick = function(){ 
			cover.src = this.getAttribute("data-cover");
			player.src = this.getAttribute("data-uri");
			player.play();
		};
	};

}


